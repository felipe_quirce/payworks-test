
package com.payworks.heroes.test;



import com.payworks.heroes.StartApplication;
import com.payworks.heroes.util.DomainBuilder;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;
import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by fquirce on 22/12/2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(StartApplication.class)
@WebIntegrationTest({"server.port=9000", "management.port=0"})
public class RestHeroAcceptanceTest {

    private static final int PORT = 9000;
    private ResponseEntity<String> response;

    @Test
    public void testDuplicateHero(){
        whenSaveRequest("spiderman","spidy","marvel","super strength", LocalDate.of(2014,10,1));
        whenSaveRequest("spiderman","spidy","marvel","super strength", LocalDate.of(2014,10,1));
        thenHTTPCode(HttpStatus.OK);
        thenResponseFailDuplicateHero();
    }

    private void thenHTTPCode(HttpStatus status) {
        assertThat(response.getStatusCode(),is(status));
    }

    private void thenResponseFailDuplicateHero() {
        assertThat(response.getBody(),containsString("Fail"));
        assertThat(response.getBody(),containsString("the request can't be completed because a hero with that name exists in DB"));
    }

    private void whenSaveRequest(String name, String pseudonym,
                                   String publisherName, String skillName, LocalDate firstApperance){
        HttpEntity entity = new HttpEntity(DomainBuilder.buildHeroJson(name,pseudonym,publisherName,skillName,firstApperance),createHeaders("user","password"));
        System.out.print(entity);
        response =new RestTemplate().postForEntity(String.format("http://localhost:%d/hero",PORT),entity ,String.class);
    }

    private HttpHeaders createHeaders(String username, String password ){
        return new HttpHeaders(){
            {
                String auth = username + ":" + password;
                byte[] encodedAuth = Base64.encodeBase64(
                        auth.getBytes(Charset.forName("US-ASCII")) );
                String authHeader = "Basic " + new String( encodedAuth );
                set( "Authorization", authHeader );
                set(CONTENT_TYPE,"application/json");
            }
        };
    }

}
