package com.payworks.heroes.controller;

import com.payworks.heroes.StartApplication;
import com.payworks.heroes.dao.HeroDAO;
import com.payworks.heroes.exception.DuplicateHeroException;
import com.payworks.heroes.rest.RestFulResponse;
import com.payworks.heroes.util.DomainBuilder;
import org.hamcrest.CoreMatchers;
import org.junit.After;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(StartApplication.class)
public class HeroeControllerIntegrationTest {
    @Autowired
    private HeroDAO heroDAO;
    @Autowired
    private HeroController victim;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    private RestFulResponse result;

    @Test
    public void testGetHeroByName_givenNonDuplicateHero_thenHeroIsReturned(){
      givenSavedHero("spiderman", LocalDate.of(2015,12,22));
        whenGetHeroByName("spiderman");
        thenResponseBodyContains("spiderman","2015-12-22");
    }

    private void givenSavedHero(String heroName, LocalDate firstApperance) {
        victim.save(DomainBuilder.buildHero(heroName,"spidy","Marvel","",firstApperance));
    }

    private void whenGetHeroByName(String spiderman) {

        result = victim.getHeroByName(spiderman);
    }

    private void thenResponseBodyContains(String... matchers) {
        assertThat(result.getCode(),is("OK"));

        assertThat(result.getBody(),allOf(Stream.of(matchers).map(CoreMatchers::containsString).collect(Collectors.toList())));
    }


    @After
    public void clearHeroes(){
        heroDAO.cleanAll();
    }

    @Test
    public void testSaveHero_givenDuplicateHero_thenReturnsOKWithFailCode(){
        givenSavedHero("spiderman", LocalDate.of(2015,12,22));
        thenExpectedExceptionTestContains("A hero with this name spiderman already exits");
        givenSavedHero("spiderman", LocalDate.of(2015,12,22));


    }

    private void thenExpectedExceptionTestContains(String expectedBody) {
        expectedException.expect(DuplicateHeroException.class);
        expectedException.expectMessage(is(expectedBody));
    }

    private void whenSaveHero(String heroName, LocalDate firstApperance) {
        result = victim.save(DomainBuilder.buildHero(heroName,"spidy","Marvel","",firstApperance));
    }

    @Test
    public void testListHeroes_givenNonDuplicateHero_thenAllHeroAreReturned(){
        givenSavedHero("spiderman", LocalDate.of(2015,12,22));
        givenSavedHero("batman", LocalDate.of(2015,12,22));
        whenListHeroes();
        thenResponseBodyContains("spiderman","2015-12-22","batman");
    }

    private void whenListHeroes() {
        result = victim.getAllHeroes();
    }


}
