package com.payworks.heroes.util;

import com.google.common.collect.Lists;
import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.domain.Publisher;
import com.payworks.heroes.domain.Skill;

import java.time.LocalDate;


public class DomainBuilder {
    private static JsonUtils jsonUtils = new JsonUtils();


    public static HeroDTO buildHero(String name, String pseudonym,
                                    String publisherName, String skillName, LocalDate firstApperance){

        HeroDTO hero = new HeroDTO();
        hero.setName(name);
        hero.setPseudonym(pseudonym);
        hero.setPublisher(new Publisher(publisherName));
        hero.setSkills(Lists.newArrayList(new Skill(skillName)));
        hero.setFirstAppearance(firstApperance);
        return hero;
    }


    public static String buildHeroJson(String name, String pseudonym,
                                String publisherName, String skillName, LocalDate firstApperance){
        HeroDTO hero = buildHero(name,pseudonym,publisherName,skillName,firstApperance);
       return jsonUtils.toJson(hero);
    }
}
