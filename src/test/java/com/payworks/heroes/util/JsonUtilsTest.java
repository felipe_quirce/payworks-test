package com.payworks.heroes.util;


import com.payworks.heroes.domain.HeroDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
@RunWith(MockitoJUnitRunner.class)
public class JsonUtilsTest {
    @InjectMocks
    private JsonUtils jsonUtils;
    private HeroDTO hero;
    private String result;
    @Test
    public void testGsonConversion_givenValidDate_thenDateFormatIsYYYYMMDD(){
        givenHeroWithNameAndAppearenceDate("batman", LocalDate.of(2015,12,21));
        whenHeroToJson();
        thenDateFormatIsCorrect();
    }

    private void thenDateFormatIsCorrect() {
        assertThat(result,containsString("2015-12-21"));

    }



    private void whenHeroToJson() {
        result = jsonUtils.toJson(hero);
    }

    private void givenHeroWithNameAndAppearenceDate(String batman, LocalDate of) {
        hero = DomainBuilder.buildHero(batman,"","","",of);
    }

}