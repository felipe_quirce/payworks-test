package com.payworks.heroes.service;

import com.payworks.heroes.dao.HeroDAO;
import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.exception.DuplicateHeroException;
import com.payworks.heroes.util.DomainBuilder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.times;

@RunWith(MockitoJUnitRunner.class)
public class HeroServiceTest {
    @Mock
    private HeroDAO heroDAO;

    @InjectMocks
    private HeroService victim;

    @Rule
    public ExpectedException expected =ExpectedException.none();

    private HeroDTO heroDTO;

    @Test
    public void test_saveHero_givenRepeatedHero_thenDuplicateHeroExceptionIsThrown(){
        givenHero();
        givenHeroWhenInDAOResponse();
        givenDuplicateHeroExceptionIsThrown();
        whenSaveHero();
        thenMockGetHeroByNameGetsCalled();


    }

    private void thenMockGetHeroByNameGetsCalled() {
        Mockito.verify(heroDAO,times(1)).getHeroByName(heroDTO.getName());
    }

    private void givenDuplicateHeroExceptionIsThrown() {
        expected.expect(DuplicateHeroException.class);
        expected.expectMessage(containsString(heroDTO.getName()));
    }

    private void whenSaveHero() {
        victim.saveHero(heroDTO);
    }

    private void givenHeroWhenInDAOResponse() {
        Mockito.when(heroDAO.getHeroByName(heroDTO.getName())).thenReturn(Optional.of(heroDTO));
    }

    private void givenHero() {
        heroDTO = DomainBuilder.buildHero("spiderman","spidey","","", LocalDate.now());
    }

    @Test
    public void test_saveHero_nonDuplicateHoer_thenSavedMethodGetsCalled(){
        givenHero();
        givenNonPresentHeroInDAOResponse();
        whenSaveHero();
        thenMockGetHeroByNameGetsCalled();
        thenMockSaveHeroGetsCalled();

    }

    private void thenMockSaveHeroGetsCalled() {
        Mockito.verify(heroDAO,times(1)).saveHero(heroDTO);
    }

    private void givenNonPresentHeroInDAOResponse() {
        Mockito.when(heroDAO.getHeroByName(heroDTO.getName())).thenReturn(Optional.empty());
    }


}