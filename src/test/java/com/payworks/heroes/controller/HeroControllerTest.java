package com.payworks.heroes.controller;

import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.rest.RestFulResponse;
import com.payworks.heroes.service.HeroService;
import com.payworks.heroes.util.DomainBuilder;
import com.payworks.heroes.util.JsonUtils;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;

/**
 * Created by fquirce on 17/12/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class HeroControllerTest {

    @InjectMocks
    private HeroController victim;

    @Mock
    private HeroService heroService;
    @Mock(answer = Answers.CALLS_REAL_METHODS)
    private JsonUtils jsonUtils;
    private HeroDTO heroDto;
    private String heroName;
    private RestFulResponse response;


    @Test
    public void testSaveHero_givenValidHero_thenHeroServiceGetsInvoked(){
        givenValidHero();
        whenSaveMethodGetsCalled();
        thenHeroServiceGetsInvoked();
    }

    private void thenHeroServiceGetsInvoked() {
        Mockito.verify(heroService,times(1)).saveHero(heroDto);
    }

    private void whenSaveMethodGetsCalled() {
        victim.save(heroDto);
    }

    private void givenValidHero() {
        heroDto = DomainBuilder.buildHero("batman","","", "",LocalDate.now());
    }


    @Test
    public void testGetHeroByName_givenUnKnownName_thenReturnsOKWithValidErrorMessage() throws Exception {
        givenHeroName("BATHERO");
        givenEmptyResponseFromService(Optional.empty());
        whenGetHeroByName();
        thenOKResponseWithBody(is("The requested hero doesn't exists in our DB"));
        thenVerifyMockServiceGetsInvoked();
    }

    private void thenVerifyMockServiceGetsInvoked() {
        Mockito.verify(heroService,times(1)).getHeroByName(heroName);
    }

    private void thenOKResponseWithBody(Matcher<String> expectedBody) {
        assertThat(response.getBody(),expectedBody);
        assertThat(response.getCode(),is("OK"));
    }

    private void whenGetHeroByName() {
        response = victim.getHeroByName(heroName);
    }

    private void givenEmptyResponseFromService(Optional<HeroDTO> serviceResponse) {
        Mockito.when(heroService.getHeroByName(heroName)).thenReturn(serviceResponse);
    }

    private void givenHeroName(String name) {
        heroName = name;

    }


    @Test
    public void testGetHeroByName_givenKnownName_thenReturnsOKWithHeroInBody() throws Exception {
        givenHeroName("batman");
        givenValidHero();
        givenEmptyResponseFromService(Optional.of(heroDto));
        whenGetHeroByName();
        thenOKResponseWithBody(containsString("batman"));
        thenVerifyMockServiceGetsInvoked();
    }
}