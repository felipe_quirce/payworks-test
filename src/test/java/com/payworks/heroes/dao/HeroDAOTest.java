package com.payworks.heroes.dao;

import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.util.DomainBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.core.Is.is;

/**
 * Created by fquirce on 17/12/2015.
 */
@RunWith(MockitoJUnitRunner.class)
public class HeroDAOTest {
    @InjectMocks
    private HeroDAO victim;
    private Optional<HeroDTO> heroResult;



    @Test
    public void testGetHeroByName_givenNotMatchingName_ThenReturnsEmptyOptional(){
        givenHeroWithName("batman");
        whenGetHeroByName("joker");
        thenResultIsEmpty();
    }

    private void thenResultIsEmpty() {
        assertThat(heroResult.isPresent(),is(false));
    }

    private void whenGetHeroByName(String name) {
        heroResult = victim.getHeroByName(name);
    }

    private void givenHeroWithName(String name) {
        victim.saveHero(DomainBuilder.buildHero(name,"","","",LocalDate.now()));

    }

    @Test
    public void testGetHeroByName_givenMatchingName_ThenReturnsCorrectHero(){
        givenHeroWithName("spiderman");
        whenGetHeroByName("spiderman");
        thenResultisNotEmpty();
        thenResultNameIs("spiderman");
    }

    private void thenResultNameIs(String excepectedName) {
        assertThat(heroResult.get(),hasProperty("name",is(excepectedName)));
    }

    private void thenResultisNotEmpty() {
        assertThat(heroResult.isPresent(),is(true));
    }

}