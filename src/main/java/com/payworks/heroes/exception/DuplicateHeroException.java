package com.payworks.heroes.exception;


public class DuplicateHeroException extends RuntimeException {

    public DuplicateHeroException() {
    }

    public DuplicateHeroException(String message) {
        super(message);
    }

    public DuplicateHeroException(String message, Throwable cause) {
        super(message, cause);
    }

    public DuplicateHeroException(Throwable cause) {
        super(cause);
    }

    public DuplicateHeroException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
