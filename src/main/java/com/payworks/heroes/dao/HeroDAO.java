package com.payworks.heroes.dao;

import com.payworks.heroes.domain.HeroDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

@Component
public class HeroDAO {
   private static Logger LOG = LoggerFactory.getLogger(HeroDAO.class);
    private static Set<HeroDTO> heroes = Collections.synchronizedSet(new HashSet<>());


    public void saveHero(HeroDTO hero){
        heroes.add(hero);
        LOG.info("hero [{}] added to the data store", hero);
    }


    public Optional<HeroDTO> getHeroByName(String heroName){
     return heroes.stream().filter(heroMatchNameDto(heroName)).findFirst();
    }


    private static Predicate<HeroDTO> heroMatchNameDto(final String heroName){
       return  (HeroDTO hero)-> Objects.equals(hero.getName(),heroName);
    }

    public Collection<HeroDTO> getAll(){
        return Collections.unmodifiableCollection(heroes);
    }


    public void cleanAll(){
        heroes.clear();
    }
}
