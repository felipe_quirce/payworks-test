package com.payworks.heroes.controller;

import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.exception.DuplicateHeroException;
import com.payworks.heroes.rest.RestFulResponse;
import com.payworks.heroes.service.HeroService;
import com.payworks.heroes.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.Optional;


@RestController
@RequestMapping(path = "/hero")
public class HeroController {
    @Autowired
    private HeroService heroService;
    @Autowired
    private JsonUtils jsonUtils;

    @RequestMapping(params = "name",method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public RestFulResponse getHeroByName(@RequestParam("name") String name) {
        final Optional<HeroDTO> heroByName = heroService.getHeroByName(name);
        String responseBody;
        if (heroByName.isPresent()) {
            responseBody = jsonUtils.toJson(heroByName.get());
        } else {
            responseBody = "The requested hero doesn't exists in our DB";
        }
        return RestFulResponse.buildOKResponseWithBody(responseBody);
    }

    @RequestMapping(method = RequestMethod.GET, produces = "application/json; charset=UTF-8")
    public RestFulResponse getAllHeroes() {
        final Collection<HeroDTO> allHeroes = heroService.getAllHeroes();
        return RestFulResponse.buildOKResponseWithBody(jsonUtils.toJson(allHeroes));
    }

    @RequestMapping(method = RequestMethod.POST, headers = {"Content-type=application/json"}, produces = "application/json; charset=UTF-8")
    public RestFulResponse save(@RequestBody HeroDTO hero) {
        heroService.saveHero(hero);
        return RestFulResponse.buildOKResponseWithBody("hero inserted properly");
    }

    @ResponseStatus(value= HttpStatus.INTERNAL_SERVER_ERROR, reason="Unexpected error on the server")
    @ExceptionHandler(Exception.class)
    public void unExpectedExceptionInTheServer () {
        // Nothing to do
    }

    @ResponseStatus(value= HttpStatus.OK)
    @ExceptionHandler(DuplicateHeroException.class)
    public RestFulResponse duplicateHeroInsertion() {
        return RestFulResponse.buildFailResponseWithBody("the request can't be completed because a hero with that name exists in DB");
    }

}
