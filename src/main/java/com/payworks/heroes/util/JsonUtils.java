package com.payworks.heroes.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.stereotype.Component;

@Component
public class JsonUtils {
    private static ObjectMapper objectMapper = new Jackson2ObjectMapperBuilder().build();

    public  String toJson(Object object){
        try {
            return objectMapper.writeValueAsString(object);
        }catch (JsonProcessingException e) {
            throw new RuntimeException("error creating json",e);
        }
    }
}
