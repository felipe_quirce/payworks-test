package com.payworks.heroes.service;

import com.payworks.heroes.dao.HeroDAO;
import com.payworks.heroes.domain.HeroDTO;
import com.payworks.heroes.exception.DuplicateHeroException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

/**
 * Created by fquirce on 17/12/2015.
 */
@Service
public class HeroService {
    @Autowired
    private HeroDAO heroDAO;

    public void saveHero(HeroDTO hero){
        final Optional<HeroDTO> heroByName = heroDAO.getHeroByName(hero.getName());
        if(heroByName.isPresent()){
            throw new DuplicateHeroException(String.format("A hero with this name %s already exits",hero.getName()));
        }else{
            heroDAO.saveHero(hero);
        }
    }


    public Optional<HeroDTO> getHeroByName(String name){
        return heroDAO.getHeroByName(name);
    }

    public Collection<HeroDTO> getAllHeroes(){
        return heroDAO.getAll();
    }

}
