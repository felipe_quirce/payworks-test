package com.payworks.heroes.domain;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.payworks.heroes.util.JsonSerializers;

import java.time.LocalDate;
import java.util.Collection;
import java.util.Objects;

public class HeroDTO {
    private String name;
    private String pseudonym;
    private Publisher publisher;
    private Collection<Skill> skills;
    private Collection<HeroDTO> allies;
    @JsonDeserialize(using = JsonSerializers.LocalDateDeserializer.class)
    @JsonSerialize(using = JsonSerializers.LocalDateSerializer.class)
    private LocalDate firstAppearance;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPseudonym() {
        return pseudonym;
    }

    public void setPseudonym(String pseudonym) {
        this.pseudonym = pseudonym;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Collection<Skill> getSkills() {
        return skills;
    }

    public void setSkills(Collection<Skill> skills) {
        this.skills = skills;
    }

    public Collection<HeroDTO> getAllies() {
        return allies;
    }

    public void setAllies(Collection<HeroDTO> allies) {
        this.allies = allies;
    }

    public LocalDate getFirstAppearance() {
        return firstAppearance;
    }

    public void setFirstAppearance(LocalDate firstAppearance) {

        this.firstAppearance = firstAppearance;
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, pseudonym, publisher, skills, allies, firstAppearance);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final HeroDTO other = (HeroDTO) obj;
        return Objects.equals(this.name, other.name)
                && Objects.equals(this.pseudonym, other.pseudonym)
                && Objects.equals(this.publisher, other.publisher)
                && Objects.equals(this.skills, other.skills)
                && Objects.equals(this.allies, other.allies)
                && Objects.equals(this.firstAppearance, other.firstAppearance);
    }


    @Override
    public String toString() {
        return "HeroDTO{" +
                "name='" + name + '\'' +
                ", pseudonym='" + pseudonym + '\'' +
                ", publisher=" + publisher +
                ", skills=" + skills +
                ", allies=" + allies +
                ", firstAppearance=" + firstAppearance +
                '}';
    }
}
