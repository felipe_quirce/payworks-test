package com.payworks.heroes.domain;

import java.util.Objects;

public class Publisher {
     private String name;

     private Publisher(){
          name="";
     }

     public Publisher(String name) {
          this.name = name;
     }

     public String getName() {
          return name;
     }

     public void setName(String name) {
          this.name = name;
     }


     @Override
     public int hashCode() {
          return Objects.hash(name);
     }

     @Override
     public boolean equals(Object obj) {
          if (this == obj) {
               return true;
          }
          if (obj == null || getClass() != obj.getClass()) {
               return false;
          }
          final Publisher other = (Publisher) obj;
          return Objects.equals(this.name, other.name);
     }
}
