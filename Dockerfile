FROM java:8
COPY build/libs/ /usr/src/myapp/
WORKDIR /usr/src/myapp
EXPOSE 8080 80
RUN mv demo*.jar demo.jar
CMD ["java","-jar","demo.jar"]